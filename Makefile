CFLAGS = -std=c++17 -fconcepts -I ./ba-graph/include/
DBGFLAGS = -g -O0 -pedantic -Wall -Wextra -DBA_GRAPH_DEBUG
FASTFLAGS = -O3 -pedantic -Wall -Wextra
COMPILE_DBG = $(CXX) $(CFLAGS) $(DBGFLAGS)
COMPILE_FAST = $(CXX) $(CFLAGS) $(FASTFLAGS)


all: test


compile: compile_test


test: compile_test
	./test.out
compile_test:
	$(COMPILE_DBG) test.cpp -o test.out

speed: compile_speed
	./speed.out
compile_speed:
	$(COMPILE_FAST) test.cpp -o speed.out

clean:
	rm -rf *.out


.PHONY: clean all compile test compile_test
