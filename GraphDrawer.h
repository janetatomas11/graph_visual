#include "basic_impl.hpp"
#include "multipoles/common_multipoles.hpp"


#include <iostream>
#include <typeinfo>

using namespace ba_graph;

class GraphDrawer
{
public:
	GraphDrawer() = default;
	~GraphDrawer() = default;

	std::string neato_representation_with_attributes(const Graph &g, 
		const std::vector<std::pair<Number, std::string>> &node_attributes = {}, 
		const std::vector<std::pair<std::pair<Number, Number>, std::string>> &edge_attributes = {});

	void visualize_with_attributes(const Graph &g, const std::string &fileName,
		const std::vector<std::pair<Number, std::string>> &node_attributes = {},
		const std::vector<std::pair<std::pair<Number, Number>, std::string>> &edge_attributes = {});
    
    Graph create_simplified(const Graph &g, const std::vector<Number> &group);
    Graph create_simplified(const Graph &g, const std::vector<std::vector<Number>> &groups);
	void simplify_and_draw(const Graph &g, 
		const std::vector<std::pair<std::vector<Number>, std::string>> &groups, 
		const std::string filename);


	void simplify_connectors(Graph &g, const Multipole &m);
	void simplify_multipole(Graph &g, const Multipole &m);
	void simplify_multipoles(Graph &g, const std::vector<Multipole> &multipoles);
	void simplify_connectors_in_multipoles(Graph &g, const std::vector<Multipole> &multipoles);
	void simplify_multipoles_and_draw(Graph &g, const std::vector<Multipole> &multipoles, 
		const std::string filename);
	
private:
    Factory factory;   
    

    void simplify(Graph &g, const std::vector<Number> &first);
    Graph copy(const Graph &g);

};