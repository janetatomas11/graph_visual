#include <iostream>
#include <vector>
#include "GraphDrawer.h"
#include <string>
#include <algorithm>
#include "io/print_nice.hpp"

using namespace ba_graph;
using namespace std;

Graph create_graph() {
	Factory f;

	vector<Vertex> nodes;

	Graph g = f.cG();

	for(int i=0;i < 14;i++) {
		nodes.push_back(f.cV());
		f.aV(g, nodes.back());
	}

	f.aE(g, nodes[0], nodes[1]);
	f.aE(g, nodes[1], nodes[2]);
	f.aE(g, nodes[2], nodes[3]);
	f.aE(g, nodes[3], nodes[0]);
	f.aE(g, nodes[3], nodes[4]);
	f.aE(g, nodes[3], nodes[5]);
	f.aE(g, nodes[4], nodes[5]);
	f.aE(g, nodes[0], nodes[6]);
	f.aE(g, nodes[0], nodes[7]);
	f.aE(g, nodes[6], nodes[7]);
	f.aE(g, nodes[8], nodes[9]);
	f.aE(g, nodes[9], nodes[10]);
	f.aE(g, nodes[1], nodes[10]);
	f.aE(g, nodes[1], nodes[8]);
	f.aE(g, nodes[11], nodes[12]);
	f.aE(g, nodes[12], nodes[13]);
	f.aE(g, nodes[2], nodes[11]);
	f.aE(g, nodes[2], nodes[13]);
	f.aE(g, nodes[2], nodes[2]);
	
	return g;
}

int main() {
	GraphDrawer drawer;
	
	Graph g = create_graph();
	drawer.visualize_with_attributes(g, "graph");

	vector<pair<Number, string>> node_attributes = {make_pair(0,"color=green, shape=ellipse"), make_pair(3, "color=green, shape=ellipse"), 
														make_pair(1, "color=yellow, shape=square"), make_pair(2, "color=yellow, shape=square")};

	pair<Number, Number> p0 = make_pair(0, 3);
	pair<Number, Number> p1 = make_pair(1, 2);

	vector<pair<pair<Number, Number>, string>> edge_attributes = {make_pair(p0, "color=red"),
																 make_pair(p1, "color=red")};

	drawer.visualize_with_attributes(g, "graph_colored", node_attributes, edge_attributes);


	vector<pair<Number, string>> node_attributes1 = {make_pair(0,"color=purple, shape=triangle"), make_pair(3, "color=purple, shape=triangle"), 
														make_pair(1, "color=yellow, shape=square"), make_pair(2, "color=yellow, shape=square")};

	vector<vector<Number>> groups = {{0, 6, 7}, {3, 4, 5}};

	g = drawer.create_simplified(g, groups);

	drawer.visualize_with_attributes(g, "graph_colored_half_simplified");

	groups = {{0, 8, 9 ,10}, {2, 11, 12, 13}};
	g = drawer.create_simplified(g, groups);

	// node_attributes1.push_back(make_pair())
	drawer.visualize_with_attributes(g, "graph_colored_simplified", node_attributes1);


}
