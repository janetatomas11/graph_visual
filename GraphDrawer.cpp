#include "GraphDrawer.h"
#include <algorithm>
#include <sstream>
#include <typeinfo>

using namespace ba_graph;

std::string GraphDrawer::neato_representation_with_attributes(const Graph &g, 
	const std::vector<std::pair<Number, std::string>> &node_attributes, 
	const std::vector<std::pair<std::pair<Number, Number>, std::string>> &edge_attributes) {
#ifdef BA_GRAPH_DEBUG
    //Check if all vertices are contained in graph
    for(const auto &p : node_attributes) {
        assert(g.contains(p.first));
    }

    //Check if all edges are contained in graph
    for(const auto &p : edge_attributes) {
		assert(g.contains(p.first.first));
		assert(g.contains(p.first.second));    	
        bool is_in_graph = g[p.first.first].find(p.first.second) != g[p.first.first].end();
        assert(is_in_graph);
    }
#endif

    std::stringstream ss;
    ss << "graph {\noverlap = false;\nsplines = false;\nsep=.3;\nnode[margin=0, fontsize=12, shape=circle, height=.3, width=.3];\n";

    for(const auto &p : node_attributes) {
        ss << "{ node [width=.3,height=.3,style=filled," + p.second + "] ";
        ss << p.first.to_int();
        ss << " }\n";
    }

    for(const auto &p : edge_attributes) {
        ss << "{ edge [" + p.second + "] ";
        ss << p.first.first.to_int();
        ss << " -- ";
        ss << p.first.second.to_int();
        ss << " }\n";
    }

    auto in_attributes = [&edge_attributes](const std::pair<Number, Number> p) {
    	for(const auto &edge : edge_attributes) {
    		if(edge.first.first == p.first && edge.first.second == p.second) {
    			return true;
    		}

    		if(edge.first.first == p.second && edge.first.second == p.first) {
    			return true;
    		}
    	}

    	return false;
    };

    for (auto ii : g.list(RP::all(), IP::primary())) {
    	if(!in_attributes(std::make_pair(ii->n1(), ii->n2()))) {
			ss << ii->n1().to_int() << " -- ";
        	ss << ii->n2().to_int() << " [id=\"";
        	ss << ii->l().index() << "\"];\n";
    	}
    }

    ss << "}";

    return ss.str();
}

void GraphDrawer::visualize_with_attributes(const Graph &g, const std::string &fileName,
	const std::vector<std::pair<Number, std::string>> &node_attributes, 
	const std::vector<std::pair<std::pair<Number, Number>, std::string>> &edge_attributes) {
     
    std::string neato_r = neato_representation_with_attributes(g, node_attributes, edge_attributes);
    neato_r.erase(std::remove(neato_r.begin(), neato_r.end(), '\n'), neato_r.end());
    std::string command = "echo '" + neato_r + "' | neato -Tpdf > '" + fileName + "'";
    if(system(command.c_str()) == -1) {
        std::cout << "error\n";
        throw std::runtime_error("Call of system() failed");
    }
}

Graph GraphDrawer::copy(const Graph &g) {
    Graph graph = factory.cG();

    for(Rotation &rot : g) {
        factory.aV(graph, rot.n());
    }

    for(Rotation &rot : g) {
        Number v = rot.n();
        std::vector<Number> neighbours = g[v].list(IP::all(), IT::n2());

        for(Number &u : neighbours) {
            if(u < v) {
                factory.aE(graph, graph[v], graph[u]);
            }
        }
    }

    for(Rotation &rot : g) {
		Number v = rot.n();
		if(g[v].find(v) != g[v].end()) {
			factory.aE(graph, graph[v], graph[v]);
		}
    }

    return graph;
}

void GraphDrawer::simplify(Graph &g, const std::vector <Number> &group) {

#ifdef BA_GRAPH_DEBUG
    //Each vertex must be contained in graph
    auto is_contained = [&g ](Number n) {
        assert(g.contains(n));
    };
    std::for_each(group.begin(), group.end(), is_contained);
#endif
    for(auto &v : group) {
        if(v != group[0]) {
            std::vector<Number> neighbours = g[v].list(IP::all(), IT::n2());

            for(auto &u : neighbours) {
                if(std::find(group.begin(), group.end(), u) == group.end() && g[group[0]].find(u) == g[group[0]].end()) {
                  factory.aE(g, g[group[0]], g[u]);
                }
            }
        }
    }

    for(auto &v : group) {
        if(v != group[0]) {
            factory.dV(g, v);
        }
    }

}

Graph GraphDrawer::create_simplified(const Graph &g, const std::vector <Number> &group) {
    Graph graph = copy(g);
    simplify(graph, group);
    return graph;
}

Graph GraphDrawer::create_simplified(const Graph &g, const std::vector<std::vector<Number>> &groups) {

#ifdef BA_GRAPH_DEBUG
	//two groups cant have common members
    std::vector<Number> group_union;
    auto check = [&group_union](const std::vector<Number> &group) {
    	for(const Number &n : group) {
    		assert(std::find(group_union.begin(), group_union.end(), n) == group_union.end());
    		group_union.push_back(n);
    	}
    };

    std::for_each(groups.begin(), groups.end(), check);
#endif


    Graph graph = copy(g);


    for(auto &group : groups) {
        simplify(graph, group);
    }

    return graph;
}

void GraphDrawer::simplify_and_draw(const Graph &g, const std::vector<std::pair<std::vector<Number>, std::string>> &groups, const std::string filename) {

    std::vector<std::pair<Number, std::string>> alternatives;
	std::vector<std::vector<Number>> store;

    for(auto &group : groups) {
    	store.push_back(group.first);
        alternatives.push_back(std::make_pair(group.first[0], group.second));
    }

    Graph graph = create_simplified(g, store);
    visualize_with_attributes(graph, filename, alternatives);
}

void GraphDrawer::simplify_multipole(Graph &g, const Multipole &m) {

#ifdef BA_GRAPH_DEBUG

    // Multipole m must be contained in g.
    assert(&m.graph() == &g);

    // At least one connector
    assert(!m.connectors.empty());

    // In each connector, at least one vertex
    for(const auto &c : m.connectors) {
        assert(!c.numbers.empty());
    }
#endif

    // We put all vertices in connectors into one vector (using new Multipole and flatten method)
    // Then we simply call simplify method

    Multipole m1 = Multipole(g, m.connectors);
    m1.flatten();
    simplify(g, m1.connectors[0].numbers);
}

void GraphDrawer::simplify_connectors(Graph &g, const Multipole &m) {

#ifdef BA_GRAPH_DEBUG

    // Multipole m must be contained in g.
    assert(&m.graph() == &g);

    // At least one connector
    assert(!m.connectors.empty());

    // In each connector, at least one vertex
    for(const auto &c : m.connectors) {
        assert(!c.numbers.empty());
    }
#endif

    // We put all vertices in connectors into one vector (using new Multipole and flatten method)
    // Then we simply call simplify method

    for(const auto &c : m.connectors) {
        simplify(g, c.numbers);
    }
}


void GraphDrawer::simplify_multipoles(Graph &g, const std::vector<Multipole> &multipoles) {

#ifdef BA_GRAPH_DEBUG
    // We must check if no vertex is contained twice in multipoles () multipoles are disjoint)
    std::set<Number> s;
    std::size_t size = 0;
    for(const auto &m : multipoles) {
        for(const auto &c : m.connectors) {
            for(const auto &n : c.numbers) {
                s.insert(n);
            }
            size += c.numbers.size();
        }
    }

    assert(s.size() == size);
#endif

    for(const auto &m: multipoles) {
        simplify_multipole(g, m);
    }
}

void GraphDrawer::simplify_connectors_in_multipoles(Graph &g, const std::vector<Multipole> &multipoles) {

#ifdef BA_GRAPH_DEBUG
    // We must check if no vertex is contained twice in multipoles () multipoles are disjoint)
    std::set<Number> s;
    std::size_t size = 0;
    for(const auto &m : multipoles) {
        for(const auto &c : m.connectors) {
            for(const auto &n : c.numbers) {
                s.insert(n);
            }
            size += c.numbers.size();
        }
    }

    assert(s.size() == size);
#endif

    for(const auto &m: multipoles) {
        simplify_connectors(g, m);
    }
}


void GraphDrawer::simplify_multipoles_and_draw(
        Graph &g, 
        const std::vector<Multipole> &multipoles,
        const std::string filename)
{

    auto join_attributes = [] (const std::vector<std::pair<int, std::string>>mapping) {
        std::string s;
        for(const auto p : mapping) {
            s += p.second;
        }
        return s;
    };

    std::vector<std::pair<Number, std::string>> node_attributes;


    for(const auto &m: multipoles) {
        node_attributes.push_back({m.connectors[0].numbers[0], join_attributes(m.node_attributes)});
        simplify_multipole(g, m);
    }

    visualize_with_attributes(g, filename, node_attributes);
}
